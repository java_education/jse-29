package ru.t1.oskinea.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! E-mail already exists...");
    }

}
