package ru.t1.oskinea.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findByLogin(@NotNull String login);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}
